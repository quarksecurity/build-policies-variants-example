LOCAL_PATH := $(call my-dir)
include $(START_POLICY)

# Alphanumeric, required, used in tarball name
VARIANT_NAME := Example_Policy_from_AOSP

# Used to indicate firmware target for this policy, optional, used in tarball name
#BUILD_NUMBER := JSS15J.I545VRUEMJ7

POLICY_DIRS := includes/aosp/policy $(LOCAL_PATH)/policy

include $(END_POLICY)
